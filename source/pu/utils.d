module pu.utils;


bool implements(Type, Interface)() @nogc nothrow
{
    import std.traits;

	bool ret = true;

	enum TypeMembers = __traits(allMembers, Type);
	enum InterfaceMembers = __traits(allMembers, Interface);

	//debug pragma(msg, TypeMembers);
	//debug pragma(msg, InterfaceMembers);

	foreach(interfaceMember; InterfaceMembers)
	{
        enum TypeHasMember = hasMember!(Type, interfaceMember);
		static if(!TypeHasMember)
		{
			pragma(msg, "Type '", Type.stringof, "' is missing member '", interfaceMember, "'");
			ret = false;
		}
		else
		{

			//debug pragma(msg, "mem: ", interfaceMember);
			alias InterfaceMemberOverloads = __traits(getOverloads, Interface, interfaceMember);
			//debug pragma(msg, "overloads: ", InterfaceMemberOverloads);
			foreach(interfaceMemberOverload; InterfaceMemberOverloads)
			{
				//debug pragma(msg, "type: ", typeof(interfaceMemberOverload).stringof);
				enum MatchFound = matchingOverloadFound!(Type, interfaceMember, interfaceMemberOverload);
				//debug pragma(msg, "Match found: ", MatchFound);
				static if(!MatchFound)
				{
					pragma(msg, "Type '", Type, "' is missing overload '", PrettyPrint!(interfaceMemberOverload, interfaceMember), "'");
					ret = false;
				}

			}
		}
	}
	return ret;
}

private template staticCat(string Separator = " ", string Prefix = "", T...)
{
	import std.conv : to;
	import std.traits;
	static if(T.length > 1)
		enum staticCat = staticCat!(Separator, Prefix, T[0]) ~ Separator ~ staticCat!(Separator, Prefix, T[1..$]);
	else static if(T.length == 1)
		static if(__traits(compiles, T.to!string))
			enum staticCat = Prefix ~ T.to!string;
		else
			enum staticCat = Prefix ~ T.stringof;
	else
		enum staticCat = "";
}

private template addSpace(string Source)
{
	static if(Source != "")
		enum addSpace = Source ~ " ";
	else
		enum addSpace = Source;
}

private template PrettyPrint(alias Fun, string Name)
{
	import std.array : replace;
	import std.conv : to;
	import std.traits;
	import std.typecons : tuple;
	private enum InterfaceOverloadAttributes = __traits(getAttributes, Fun);
	private enum InterfaceOverloadFuncAttributes = functionAttributes!Fun;
	private enum InterfaceOverloadFuncLinkage = functionLinkage!Fun;
	private alias InterfaceOverloadRetType = ReturnType!Fun;
	private alias InterfaceOverloadParams = Parameters!Fun;
	private enum InterfaceOverloadParamIdentifiers = ParameterIdentifierTuple!Fun;
	private enum InterfaceOverloadParamStorageClasses = ParameterStorageClassTuple!Fun;
	private enum InterfaceOverloadIsStatic = __traits(isStaticFunction, Fun);
	private enum FuncUDAs = staticCat!(" ", "@", InterfaceOverloadAttributes);
	private enum FuncAttributes = staticCat!(" ", "@", InterfaceOverloadFuncAttributes);
	private enum FuncLinkage = InterfaceOverloadFuncLinkage;
	private enum FuncIsStatic = InterfaceOverloadIsStatic ? "static" : "";
	private enum FuncParams = fqParams!(stringify!InterfaceOverloadParams, [InterfaceOverloadParamStorageClasses], [InterfaceOverloadParamIdentifiers]);
	enum PrettyPrint = addSpace!(FuncUDAs) ~ addSpace!(FuncIsStatic) ~ InterfaceOverloadRetType.stringof ~ " " ~ Name ~ "(" ~ staticCat!(", ", "", InterfaceOverloadParams) ~ ") " ~ FuncAttributes;
}

private bool matchingOverloadFound(Type, string InterfaceFunName, alias InterfaceFunOverload)()
{
	import std.traits;

	bool ret = false;
	alias TypeMemberOverloads = __traits(getOverloads, Type, InterfaceFunName);
	static foreach(typeMemberOverload; TypeMemberOverloads)
	{
		enum InterfaceOverloadAttributes = __traits(getAttributes, InterfaceFunOverload);
		enum InterfaceOverloadFuncAttributes = functionAttributes!InterfaceFunOverload;
		enum InterfaceOverloadFuncLinkage = functionLinkage!InterfaceFunOverload;
		alias InterfaceOverloadRetType = ReturnType!InterfaceFunOverload;
		alias InterfaceOverloadParams = Parameters!InterfaceFunOverload;
		enum InterfaceOverloadParamStorageClasses = ParameterStorageClassTuple!InterfaceFunOverload;
		enum InterfaceOverloadIsStatic = __traits(isStaticFunction, InterfaceFunOverload);
		//debug pragma(msg, "interface member overload attributes: ", InterfaceOverloadAttributes);
		//debug pragma(msg, "interface member overload function attributes: ", InterfaceOverloadFuncAttributes);
		//debug pragma(msg, "interface member overload function linkage: ", InterfaceOverloadFuncLinkage);
		//debug pragma(msg, "interface member overload return type: ", InterfaceOverloadRetType);
		//debug pragma(msg, "interface member overload parameters: ", InterfaceOverloadParams);
		//debug pragma(msg, "interface member overload is static: ", InterfaceOverloadIsStatic);

		enum TypeOverloadAttributes = __traits(getAttributes, typeMemberOverload);
		enum TypeOverloadFuncAttributes = functionAttributes!typeMemberOverload;
		enum TypeOverloadFuncLinkage = functionLinkage!typeMemberOverload;
		alias TypeOverloadRetType = ReturnType!typeMemberOverload;
		alias TypeOverloadParams = Parameters!typeMemberOverload;
		enum TypeOverloadParamStorageClasses = ParameterStorageClassTuple!typeMemberOverload;
		enum TypeOverloadIsStatic = __traits(isStaticFunction, typeMemberOverload);
		//debug pragma(msg, "type member overload attributes: ", TypeOverloadAttributes);
		//debug pragma(msg, "type member overload function attributes: ", TypeOverloadFuncAttributes);
		//debug pragma(msg, "type member overload function linkage: ", TypeOverloadFuncLinkage);
		//debug pragma(msg, "type member overload return type: ", TypeOverloadRetType);
		//debug pragma(msg, "type member overload parameters: ", TypeOverloadParams);
		//debug pragma(msg, "type member overload is static: ", TypeOverloadIsStatic);
		//if(!is(TypeOverloadAttributes == InterfaceOverloadAttributes))
		//	pragma(msg, "attributes don't match");
		//if(!(TypeOverloadFuncAttributes == InterfaceOverloadFuncAttributes))
		//	pragma(msg, "func attributes don't match");
		//if(!(TypeOverloadFuncLinkage == InterfaceOverloadFuncLinkage))
		//	pragma(msg, "func linkage doesn't match");
		//if(!is(TypeOverloadRetType == InterfaceOverloadRetType))
		//	pragma(msg, "return type doesn't match");
		//if(!is(TypeOverloadParams == InterfaceOverloadParams))
		//	pragma(msg, "params don't match");
		//if(!(TypeOverloadIsStatic == InterfaceOverloadIsStatic))
		//	pragma(msg, InterfaceOverloadIsStatic ? "overload isn't static" : "overload should be static");
		//if(is(TypeOverloadAttributes == InterfaceOverloadAttributes)
		//   && TypeOverloadFuncAttributes == InterfaceOverloadFuncAttributes
		//   && TypeOverloadFuncLinkage == InterfaceOverloadFuncLinkage
		//   && is(TypeOverloadRetType == InterfaceOverloadRetType)
		//   && is(TypeOverloadParams == InterfaceOverloadParams)
		//   && InterfaceOverloadIsStatic == TypeOverloadIsStatic)
		static if(TypeOverloadAttributes.length != InterfaceOverloadAttributes.length)
		{
			debug pragma(msg, InterfaceFunName, ": attribute lengths don't match");
		}
		else static if(TypeOverloadAttributes != InterfaceOverloadAttributes)
		{
			debug pragma(msg, InterfaceFunName, ": attributes don't match");
		}
		else static if(TypeOverloadFuncAttributes != InterfaceOverloadFuncAttributes)
		{
			debug pragma(msg, InterfaceFunName, ": function attributes don't match");
		}
		else static if(TypeOverloadFuncLinkage != InterfaceOverloadFuncLinkage)
		{
			debug pragma(msg, InterfaceFunName, ": function linkage doesn't match");
		}
		else static if(!is(TypeOverloadRetType == InterfaceOverloadRetType))
		{
			debug pragma(msg, InterfaceFunName, ": function return type doesn't match");
		}
		else static if(TypeOverloadParams.length != InterfaceOverloadParams.length)
		{
			debug pragma(msg, InterfaceFunName, ": function param lengths don't match");
		}
		else static if(!is(TypeOverloadParams == InterfaceOverloadParams))
		{
			debug pragma(msg, InterfaceFunName, ": function params don't match");
		}
		else static if(TypeOverloadParamStorageClasses != InterfaceOverloadParamStorageClasses)
		{
			debug pragma(msg, InterfaceFunName, ": function param storage classes don't match");
		}
		else static if(TypeOverloadIsStatic != InterfaceOverloadIsStatic)
		{
			debug pragma(msg, InterfaceFunName, InterfaceOverloadIsStatic ? ": function isn't static" : ": function should not be static");
		}
		else static if(is(typeof(&typeMemberOverload) == typeof(&InterfaceFunOverload)))
		{
			//debug pragma(msg, InterfaceFunName, ": overload found");
			ret = true;
		}
	}

	return ret;
}

import std.traits : ParameterStorageClass;
private string fqParams(string[] Params, ParameterStorageClass[] StorageClasses, string[] Identifiers)()
{
	string ret;
	static foreach(i, param; Params)
	{
		static if(i > 0)
			ret ~= ", ";
		ret ~= fullParam!(param, StorageClasses[i], Identifiers[i]);
	}
	return ret;
}

private string fullParam(string Param, ParameterStorageClass StorageClasses, string Identifier)()
{
	import std.conv : to;
	return StorageClasses.to!string ~ " " ~ Param ~ " " ~ Identifier;
}

private template stringify(T...)
{
	static if(T.length > 1)
		static immutable stringify = stringify!(T[0]) ~ stringify!(T[1..$]);
	else static if(T.length == 1)
		static immutable stringify = [T[0].stringof];
	else
		static immutable stringify = [];
}
