module pu.array;

import pu.alloc;
import pu.assert_;

/// TODO
/// fix opIndexOpAssign, currently opIndex seems to be used for `arr[index] op= value`;

struct DynArray(T) if (!is(T == class))
{
    private
    {
        T* _ptr;
        size_t _size;
        size_t _capacity;
    }

    ~this() @nogc nothrow
    {
        if (_ptr)
        {
            static if (is(T == struct))
                foreach (ref v; _ptr[0 .. _size])
                    destroy!false(v);

            free(_ptr);
            _ptr = null;
            _size = 0;
            _capacity = 0;
        }
    }

    this(size_t size, size_t capacity = 0) @nogc nothrow
    {
        auto requestSize = size;
        if (capacity > size)
            requestSize = capacity;

        auto ptr = allocArray!T(requestSize);
        ASSERT(ptr, "failed to allocate array");
        _ptr = ptr;
        _size = size;
        _capacity = requestSize - capacity;
    }

    this(typeof(this) other) @nogc nothrow
    {
        // Log.dbg("array: __ctor(typeof(this) other)");
        auto ptr = allocArray!T(other._capacity);
        ASSERT(ptr, "failed to allocate array");
        _ptr = ptr;
        _size = other._size;
        _capacity = other._capacity;
        foreach (i; 0 .. _size)
            _ptr[i] = other._ptr[i];
        //_ptr[0 .. _size] = other._ptr[0 .. _size];
    }

    this(T[] values) @nogc nothrow
    {
        auto ptr = allocArray!T(values.length);
        ASSERT(ptr, "failed to allocate array");
        _ptr = ptr;
        _size = values.length;
        _capacity = values.length;
        foreach (i; 0 .. _size)
            _ptr[i] = values[i];
    }

    this(this) @nogc nothrow
    {
        // Log.dbg("array: __postblit");
        auto newPtr = allocArray!T(_capacity);
        ASSERT(newPtr, "failed to allocate array while cloning");
        //newPtr[0 .. _size] = _ptr[0 .. _size];
        foreach (i; 0 .. _size)
            newPtr[i] = _ptr[i];
        _ptr = newPtr;
    }

    size_t length() const @nogc nothrow
    {
        return _size;
    }

    void clear() @nogc nothrow
    {
        foreach (i; 0 .. _size)
            _ptr[i] = T.init;
        _size = 0;
    }

    ref T opIndex(size_t idx) @nogc nothrow
    {
        ASSERT(_ptr);
        ASSERT(idx < _size);
        return _ptr[idx];
    }

    T[] opIndex() @nogc nothrow
    {
        ASSERT(_ptr);
        return _ptr[0 .. _size];
    }

    size_t[2] opSlice(size_t dim : 0)(size_t from, size_t to) @nogc nothrow
    {
        ASSERT(_ptr);
        ASSERT(from >= 0);
        ASSERT(to <= _size);
        ASSERT(from <= to);
        return [from, to];
    }

    size_t opDollar(size_t pos : 0)() @nogc nothrow
    {
        return _size;
    }

    void opIndexAssign(T value) @nogc nothrow
    {
        ASSERT(_ptr);
        foreach (i; 0 .. _size)
            _ptr[i] = value;
        //foreach (ref v; _ptr[0 .. _size])
        //    v = value;
    }

    void opIndexAssign(T[] values) @nogc nothrow
    {
        ASSERT(_ptr);
        ASSERT(values.length == _size);
        foreach (i; 0 .. _size)
            _ptr[i] = values[i];
        //_ptr[0 .. _size] = values;
    }

    void opIndexAssign(T value, size_t index) @nogc nothrow
    {
        ASSERT(_ptr);
        ASSERT(index < _size);
        _ptr[index] = value;
    }

    void opIndexAssign(T value, size_t[2] sliceIndices) @nogc nothrow
    {
        ASSERT(_ptr);
        ASSERT(sliceIndices[0] >= 0);
        ASSERT(sliceIndices[1] <= _size);
        ASSERT(sliceIndices[0] <= sliceIndices[1]);
        foreach (i; sliceIndices[0] .. sliceIndices[1])
            _ptr[i] = value;
        //foreach (ref v; _ptr[sliceIndices[0] .. sliceIndices[1]])
        //    v = value;
        //_ptr[sliceIndices[0]..sliceIndices[1]] = value;
    }

    void opIndexAssign(T[] values, size_t[2] sliceIndices) @nogc nothrow
    {
        ASSERT(_ptr);
        ASSERT(sliceIndices[0] >= 0);
        ASSERT(sliceIndices[1] <= _size);
        ASSERT(sliceIndices[0] <= sliceIndices[1]);
        ASSERT(values.length == (sliceIndices[1] - sliceIndices[0]));
        foreach (i; sliceIndices[0] .. sliceIndices[1])
            _ptr[i] = values[i - sliceIndices[0]];
        //_ptr[sliceIndices[0] .. sliceIndices[1]] = values;
    }

    void opOpAssign(string op : "~")(T value) @nogc nothrow
    {
        push(value);
    }

    void opOpAssign(string op : "~")(T[] values) @nogc nothrow
    {
        foreach (val; values)
            push(val);
    }

    void opIndexOpAssign(string op)(T value) @nogc nothrow
    {
        ASSERT(_ptr);
        foreach (i; 0 .. _size)
            mixin("_ptr[i] " ~ op ~ "= value");
    }

    void opIndexOpAssign(string op)(T value, size_t index) @nogc nothrow
    {
        ASSERT(_ptr);
        ASSERT(index < _size);
        mixin("_ptr[index] " ~ op ~ "= value;");
    }

    void opIndexOpAssign(string op)(T value, size_t[2] sliceIndices) @nogc nothrow
    {
        ASSERT(_ptr);
        ASSERT(sliceIndices[0] >= 0);
        ASSERT(sliceIndices[1] <= _size);
        ASSERT(sliceIndices[0] <= sliceIndices[1]);
        foreach (i; sliceIndices[0] .. sliceIndices[1])
            mixin("_ptr[i] " ~ op ~ "= value;");
    }

    T remove(size_t idx) @nogc nothrow
    {
        ASSERT(_ptr);
        ASSERT(idx < _size);
        auto ret = _ptr[idx];
        foreach (i; idx .. _size - 1)
            _ptr[i] = _ptr[i + 1];
        //_ptr[idx .. _size - 1] = _ptr[idx + 1 .. _size];
        _size -= 1;
        return ret;
    }

    T swap_remove(size_t idx) @nogc nothrow
    {
        ASSERT(_ptr);
        ASSERT(idx < _size);
        auto ret = _ptr[idx];
        _ptr[idx] = _ptr[_size - 1];
        _size -= 1;
        return ret;
    }

    T pop() @nogc nothrow
    {
        ASSERT(_ptr);
        ASSERT(_size > 0);
        _size -= 1;
        return _ptr[_size];
    }

    void push(T val) @nogc nothrow
    {
        ASSERT(_ptr);

        import pu.log;

        Log.dbg("DynArray: pushing val: %s", T.stringof);

        if (_size == _capacity)
            _resize();

        //ASSERT(_ptr[_size] == T.init);
        import core.lifetime : moveEmplace;

        //_ptr[_size] = val;
        moveEmplace(val, _ptr[_size]);
        _size += 1;
    }

    void resize(size_t new_size) @nogc nothrow
    {
        ASSERT(_ptr);
        if (new_size == _size)
        {
            return;
        }
        else if (new_size < _size)
        {
            _ptr[new_size .. _size] = T.init;
            _size = new_size;
        }
        else if (new_size <= _capacity)
        {
            _size = new_size;
        }
        else // new_size > capacity
        {
            _resize(new_size);
            _size = new_size;
        }
    }

    private void _resize(size_t _new_capacity = 0) @nogc nothrow
    {
        import pu.log;
        import std.algorithm : max;

        auto new_capacity = max(1, max(_new_capacity, _capacity)) * 2;
        Log.dbg("DynArray: resizing capacity from %s to %s", _capacity, new_capacity);
        auto new_arr = allocArray!T(new_capacity);
        ASSERT(new_arr);
        foreach (i; 0 .. _size)
            new_arr[i] = _ptr[i];
        //new_arr[0 .. _size] = _ptr[0 .. _size];
        free(_ptr);
        _ptr = new_arr;
        _capacity = new_capacity;
    }
}

struct Array(T) if (!is(T == class))
{
    import pu.log;

    private
    {
        T* _ptr = null;
        size_t _size = 0;
    }

    ~this() @nogc nothrow
    {
        // Log.dbg("array __dtor");
        if (_ptr)
        {
            // Log.dbg("array: detroying _ptr: %s", _ptr);
            static if (is(T == struct))
                foreach (i; 0 .. _size)
                    {
                    // Log.dbg("array: running destroy on: %s", i);
                    destroy!false(_ptr[i]);
                }
            //foreach (ref v; _ptr[0 .. _size])
            //    destroy!false(v);
            free(_ptr);
            _ptr = null;
            _size = 0;
        }
    }

    this(size_t size) @nogc nothrow
    {
        // Log.dbg("array: __ctor(%s)", size);
        auto ptr = allocArray!T(size);
        ASSERT(ptr, "failed to allocate array");
        _ptr = ptr;
        _size = size;
    }

    this(typeof(this) other) @nogc nothrow
    {
        // Log.dbg("array: __ctor(typeof(this) other)");
        auto ptr = allocArray!T(other._size);
        ASSERT(ptr, "failed to allocate array");
        _ptr = ptr;
        _size = other._size;
        foreach (i; 0 .. _size)
            _ptr[i] = other._ptr[i];
        //_ptr[0 .. _size] = other._ptr[0 .. _size];
    }

    this(T[] values) @nogc nothrow
    {
        auto ptr = allocArray!T(values.length);
        ASSERT(ptr, "failed to allocate array");
        _ptr = ptr;
        _size = values.length;
        foreach (i; 0 .. _size)
            _ptr[i] = values[i];
    }

    this(this) @nogc nothrow
    {
        // Log.dbg("array: __postblit");
        auto newPtr = allocArray!T(_size);
        ASSERT(newPtr, "failed to allocate array while cloning");
        //newPtr[0 .. _size] = _ptr[0 .. _size];
        foreach (i; 0 .. _size)
            newPtr[i] = _ptr[i];
        _ptr = newPtr;
    }

    size_t length() const @nogc nothrow
    {
        return _size;
    }

    ref T opIndex(size_t idx) @nogc nothrow
    {
        ASSERT(_ptr);
        ASSERT(idx < _size);
        return _ptr[idx];
    }

    T[] opIndex() @nogc nothrow
    {
        //ASSERT(_ptr);
        return _ptr[0 .. _size];
    }

    size_t[2] opSlice(size_t dim : 0)(size_t from, size_t to) @nogc nothrow
    {
        ASSERT(_ptr);
        ASSERT(from >= 0);
        ASSERT(to <= _size);
        ASSERT(from <= to);
        return [from, to];
    }

    size_t opDollar(size_t pos : 0)() @nogc nothrow
    {
        return _size;
    }

    void opIndexAssign(T value) @nogc nothrow
    {
        ASSERT(_ptr);
        foreach (i; 0 .. _size)
            _ptr[i] = value;
    }

    void opIndexAssign(T[] values) @nogc nothrow
    {
        ASSERT(_ptr);
        ASSERT(values.length == _size);
        foreach (i; 0 .. _size)
            _ptr[i] = values[i];
        //_ptr[0 .. _size] = values;
    }

    void opIndexAssign(T value, size_t index) @nogc nothrow
    {
        ASSERT(_ptr);
        ASSERT(index < _size);
        _ptr[index] = value;
    }

    void opIndexAssign(T value, size_t[2] sliceIndices) @nogc nothrow
    {
        ASSERT(_ptr);
        ASSERT(sliceIndices[0] >= 0);
        ASSERT(sliceIndices[1] <= _size);
        ASSERT(sliceIndices[0] <= sliceIndices[1]);
        foreach (i; sliceIndices[0] .. sliceIndices[1])
            _ptr[i] = value;
        //foreach (ref v; _ptr[sliceIndices[0] .. sliceIndices[1]])
        //    v = value;
        //_ptr[sliceIndices[0]..sliceIndices[1]] = value;
    }

    void opIndexAssign(T[] values, size_t[2] sliceIndices) @nogc nothrow
    {
        ASSERT(_ptr);
        ASSERT(sliceIndices[0] >= 0);
        ASSERT(sliceIndices[1] <= _size);
        ASSERT(sliceIndices[0] <= sliceIndices[1]);
        ASSERT(values.length == (sliceIndices[1] - sliceIndices[0]));
        //_ptr[sliceIndices[0] .. sliceIndices[1]] = values;
        foreach (i; sliceIndices[0] .. sliceIndices[1])
            _ptr[i] = values[i - sliceIndices[0]];
    }
}
