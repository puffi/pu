module pu.log;

import core.stdc.stdio : FILE, fopen, stderr;
import pu.assert_;

import std.traits : isIntegral, isFloatingPoint;

@nogc nothrow:

private enum FormatBufferSize = 4096 - 1;

private __gshared LogImpl _logImpl;

enum LogLevel
{
    err,
    warn,
    info,
    dbg,
}

/// not thread-safe
struct Log
{
    static bool open(string logfile, LogLevel level) @nogc nothrow
    {
        if (_logImpl.file != null)
            return true;

        if (logfile == "-")
            _logImpl.file = stderr;
        else
        {
            char[256] buf;
            ASSERT(logfile.length < 256);
            buf[0 .. logfile.length] = logfile[];
            buf[logfile.length] = '\0';
            auto file = fopen(buf.ptr, "w");
            ASSERT(file != null, "failed to open logfile");
            _logImpl.file = file;
        }
        _logImpl.level = level;

        return true;
    }

    static void err(Args...)(string fmt, Args args, string file = __FILE__, size_t line = __LINE__) @nogc nothrow
    {
        if (_logImpl.level < LogLevel.err)
            return;

        _log(LogLevel.err, file, line, fmt, args);
        //auto prefixSize = fillLogPrefix(fmtBuffer, file, line, LogLevel.err);
        //import std.format : sformat;

        //auto msg = sformat(fmtBuffer[prefixSize .. $], fmt, args);
        //_logImpl.writeln(fmtBuffer[0 .. prefixSize + msg.length]);
    }

    static void warn(Args...)(string fmt, Args args, string file = __FILE__, size_t line = __LINE__) @nogc nothrow
    {
        if (_logImpl.level < LogLevel.warn)
            return;

        _log(LogLevel.warn, file, line, fmt, args);
        //import std.format : sformat;

        //auto msg = sformat(fmtBuffer[prefixSize .. $], fmt, args);
        //_logImpl.writeln(fmtBuffer[0 .. prefixSize + msg.length]);
    }

    static void info(Args...)(string fmt, Args args, string file = __FILE__, size_t line = __LINE__) @nogc nothrow
    {
        if (_logImpl.level < LogLevel.info)
            return;

        _log(LogLevel.info, file, line, fmt, args);
        //auto prefixSize = fillLogPrefix(fmtBuffer, file, line, LogLevel.info);
        //import std.format : sformat;

        //auto msg = sformat(fmtBuffer[prefixSize .. $], fmt, args);
        //_logImpl.writeln(fmtBuffer[0 .. prefixSize + msg.length]);
    }

    static void dbg(Args...)(string fmt, Args args, string file = __FILE__, size_t line = __LINE__) @nogc nothrow
    {
        if (_logImpl.level < LogLevel.dbg)
            return;

        _log(LogLevel.dbg, file, line, fmt, args);
        //import core.stdc.stdio : fprintf;

        //auto prefixSize = fillLogPrefix(fmtBuffer, file, line, LogLevel.dbg);
        ////fprintf(_logImpl.file, "%.*s", cast(int) prefixSize, fmtBuffer.ptr);

        ////auto fmtInput = fmt;
        ////size_t index = 0;
        ////while (!fmtInput.empty)
        ////{
        ////    auto count = fmtInput.byCodeUnit.countUntil('%');
        ////    fprintf(_logImpl.file, "%.*s", cast(int) count, fmtInput.ptr);
        ////    // TODO parse format
        ////}
        //FmtRange fmtRange = FmtRange(fmt);
        //Formatter formatter = Formatter(fmtBuffer, prefixSize);

        //foreach (arg; args)
        //{
        //    if (fmtRange.prefix)
        //        formatter.formatValue(fmtRange.prefix, "%s");
        //    if (fmtRange.fmt)
        //        formatter.formatValue(arg, fmtRange.fmt);
        //    else
        //        formatter.formatValue(arg, "%s");
        //    fmtRange.findNextFmt();
        //}
        //while (!fmtRange.empty)
        //{
        //    if (fmtRange.prefix)
        //        formatter.formatValue(fmtRange.prefix, "%s");
        //    if (fmtRange.fmt)
        //        ASSERT(false, "found format string without argument");
        //    fmtRange.findNextFmt();
        //}
        //_logImpl.writeln(formatter);

        //auto fmtRange = FormatRange(fmt);
        //foreach (arg; args)
        //{
        //    while (!fmtRange.empty)
        //    {
        //        final switch (fmtRange.front.type) with (Format.Type)
        //        {
        //        case invalid:
        //            fprintf(_logImpl.file, "INVALID FORMAT SPECIFIER: %.*s",
        //                    cast(int) fmtRange.front.value.length, fmtRange.front.value.ptr);
        //            break;
        //        case literal:
        //            fprintf(_logImpl.file, "%.*s",
        //                    cast(int) fmtRange.front.value.length, fmtRange.front.value.ptr);
        //            break;
        //        case asString:
        //            break;
        //        case asUnsigned:
        //            static if (isIntegral!(typeof(arg)))
        //            {

        //                static if (arg.sizeof == 1)
        //                {
        //                    enum format = "%hhu";
        //                }
        //                else static if (arg.sizeof == 2)
        //                {
        //                    enum format = "%hu";
        //                }
        //                else static if (arg.sizeof == 4)
        //                {
        //                    enum format = "%u";
        //                }
        //                else static if (arg.sizeof == 8)
        //                {
        //                    enum format = "%llu";
        //                }
        //                else
        //                    static assert(false, "no valid size for type: " ~ typeof(arg).stringof);
        //                fpritnf(_logImpl.file, format, arg);
        //            }
        //            else
        //            {
        //                fprintf(_logImpl.file, "INVALID FORMAT FOR TYPE: %.*s",
        //                        typeof(arg).stringof.length, typeof(arg).stringof);
        //            }
        //            break;
        //        case asSigned:
        //            static if (isIntegral!(typeof(arg)))
        //            {

        //                static if (arg.sizeof == 1)
        //                {
        //                    enum format = "%hhd";
        //                }
        //                else static if (arg.sizeof == 2)
        //                {
        //                    enum format = "%hd";
        //                }
        //                else static if (arg.sizeof == 4)
        //                {
        //                    enum format = "%d";
        //                }
        //                else static if (arg.sizeof == 8)
        //                {
        //                    enum format = "%lld";
        //                }
        //                else
        //                    static assert(false, "no valid size for type: " ~ typeof(arg).stringof);
        //                fpritnf(_logImpl.file, format, arg);
        //            }
        //            else
        //            {
        //                fprintf(_logImpl.file, "INVALID FORMAT FOR TYPE: %.*s",
        //                        typeof(arg).stringof.length, typeof(arg).stringof);
        //            }
        //            break;
        //        case asHex:
        //            static if (isIntegral!(typeof(arg)) || isFloatingPoint!(typeof(arg)))
        //            {

        //                static if (arg.sizeof == 1)
        //                {
        //                    enum format = "%hhx";
        //                }
        //                else static if (arg.sizeof == 2)
        //                {
        //                    enum format = "%hx";
        //                }
        //                else static if (arg.sizeof == 4)
        //                {
        //                    enum format = "%x";
        //                }
        //                else static if (arg.sizeof == 8)
        //                {
        //                    enum format = "%llx";
        //                }
        //                else
        //                    static assert(false, "no valid size for type: " ~ typeof(arg).stringof);
        //                fpritnf(_logImpl.file, format, arg);
        //            }
        //            else
        //            {
        //                fprintf(_logImpl.file, "INVALID FORMAT FOR TYPE: %.*s",
        //                        typeof(arg).stringof.length, typeof(arg).stringof);
        //            }
        //            break;
        //        }
        //        fmtRange.popFront();
        //    }
        //}
        //while (!fmtRange.empty)
        //{
        //    switch (fmtRange.front.type) with (Format.Type)
        //    {
        //    case literal:
        //        fprintf(_logImpl.file, "%.*s",
        //                cast(int) fmtRange.front.value.length, fmtRange.front.value.ptr);
        //        break;
        //    default:
        //        fprintf(_logImpl.file, "MISSING FORMAT VALUE FOR FMT: %.*s",
        //                cast(int) fmtRange.front.value.length, fmtRange.front.value.ptr);
        //        break;
        //    }
        //    fmtRange.popFront();
        //}
        //fprintf(_logImpl.file, "\n");

        //_logImpl.write(fmtBuffer[0..prefixSize]);
        //fmtBuffer[prefixSize..prefixSize+fmt.length] = fmt;
        //_logImpl.writeln(fmtBuffer[0..prefixSize+fmt.length]);
        //import std.format : sformat;

        //auto msg = sformat(fmtBuffer[prefixSize .. $], fmt, args);
        //_logImpl.writeln(fmtBuffer[0 .. prefixSize + msg.length]);
    }

    private static _log(Args...)(LogLevel level, string file, size_t line, string fmt, Args args) @nogc nothrow
    {
        static char[FormatBufferSize] fmtBuffer;

        auto prefixSize = fillLogPrefix(fmtBuffer, file, line, level);

        FmtRange fmtRange = FmtRange(fmt);
        Formatter formatter = Formatter(fmtBuffer, prefixSize);

        foreach (arg; args)
        {
            if (fmtRange.prefix)
                formatter.formatValue(fmtRange.prefix, "%s");
            if (fmtRange.fmt)
                formatter.formatValue(arg, fmtRange.fmt);
            else
                formatter.formatValue(arg, "%s");
            fmtRange.findNextFmt();
        }
        while (!fmtRange.empty)
        {
            if (fmtRange.prefix)
                formatter.formatValue(fmtRange.prefix, "%s");
            if (fmtRange.fmt)
                ASSERT(false, "found format string without argument");
            fmtRange.findNextFmt();
        }
        _logImpl.writeln(formatter);
    }
}

private struct LogImpl
{
    FILE* file;
    LogLevel level;
    string prefix;
    uint lock;

    void write(Formatter formatter) @nogc nothrow
    {
        import core.stdc.stdio : fprintf;

        fprintf(_logImpl.file, "%.*s", cast(int) formatter.count, formatter.buffer.ptr);
    }

    void writeln(Formatter formatter) @nogc nothrow
    {
        write(formatter);
        import core.stdc.stdio : fprintf;

        fprintf(_logImpl.file, "\n");
    }

    void write(const(char)[] msg) @nogc nothrow
    {
        import core.stdc.stdio : fprintf;

        ASSERT(msg.length < int.max, "log msg too long");
        fprintf(_logImpl.file, "%.*s", cast(int) msg.length, msg.ptr);
    }

    void writeln(const(char)[] msg) @nogc nothrow
    {
        import core.stdc.stdio : fprintf;

        ASSERT(msg.length < int.max, "log msg too long");
        fprintf(_logImpl.file, "%.*s\n", cast(int) msg.length, msg.ptr);
    }

    void close() @nogc nothrow
    {
        import core.stdc.stdio : fprintf, stdout, fclose;

        if (file)
        {
            fprintf(stdout, "closing logfile\n");
            ASSERT(!fclose(file), "failed to close logfile");
        }
    }

    ~this() @nogc nothrow
    {
        close();
    }
}

private size_t fillTimestamp(char[] buf) @nogc nothrow
{
    import core.stdc.time : time, time_t, tm, strftime;
    import core.sys.posix.time : localtime_r;

    time_t now;
    time(&now);
    tm now_tm;
    auto res = localtime_r(&now, &now_tm);
    ASSERT(res != null, "failed to get localtime_r");
    auto count = strftime(buf.ptr, buf.length, "%FT%T", res);
    if (res.tm_gmtoff == 0)
        buf[count++] = 'Z';
    else
    {
        char sign = res.tm_gmtoff > 0 ? '+' : '-';
        buf[count++] = sign;

        import std.conv : toChars;

        auto hours = res.tm_gmtoff / 3600;
        auto hoursChars = hours.toChars;
        if (hoursChars.length < 2)
            buf[count++] = '0';
        foreach (c; hoursChars)
            buf[count++] = c;

        buf[count++] = ':';

        auto minutes = (res.tm_gmtoff / 60) % 60;
        auto minutesChars = minutes.toChars;
        if (minutesChars.length < 2)
            buf[count++] = '0';
        foreach (c; minutesChars)
            buf[count++] = c;
    }

    return count;
    //static struct OutputRangeWrapper
    //{
    //	char[] _buf;

    //	void put(char c)
    //	{
    //		ASSERT(_buf.length > 0, "timestamp output buffer too small");
    //		_buf[0] = c;
    //		_buf = _buf[1..$];
    //	}
    //}

    //import std.datetime : Clock, SysTime;

    //auto now = Clock.currTime;
    //char[33] rfc3339_buf = '\0';
    //auto wrapper = OutputRangeWrapper(rfc3339_buf[]);
    //now.toISOExtString(wrapper, 7);
    //import std.algorithm : countUntil;

    //auto len = rfc3339_buf[].countUntil('\0');
    //buf[0 .. len] = rfc3339_buf[0 .. len];
    //return len;
}

private size_t fillLogPrefix(char[] buf, string file, size_t line, LogLevel level) @nogc nothrow
{
    size_t count;
    count += fillTimestamp(buf);
    buf[count++] = ' ';

    final switch (level) with (LogLevel)
    {
    case err:
        buf[count .. count + "ERROR".length] = "ERROR";
        count += "ERROR".length;
        break;
    case warn:
        buf[count .. count + "WARN".length] = "WARN";
        count += "WARN".length;
        break;
    case info:
        buf[count .. count + "INFO".length] = "INFO";
        count += "INFO".length;
        break;
    case dbg:
        buf[count .. count + "DEBUG".length] = "DEBUG";
        count += "DEBUG".length;
        break;
    }
    buf[count++] = ' ';

    buf[count++] = '(';
    buf[count .. count + file.length] = file;
    count += file.length;
    buf[count++] = ':';

    import std.conv : toChars;

    auto lineChars = line.toChars;
    foreach (c; lineChars)
        buf[count++] = c;
    //buf[count..lineChars.length] = lineChars;
    //count += lineChars.length;
    buf[count++] = ')';
    buf[count++] = ':';
    buf[count++] = ' ';

    return count;
}

// fmt:
// u -> unsigned
// d -> signed
// x -> hex
private size_t fillInteger(T)(char[] buf, T val, string fmt) @nogc nothrow 
        if (isIntegral!T)
{
    size_t count = 0;

    return count;
}

enum NumberRepr : ubyte
{
    decimal,
    hex,
    octal,
    binary
}

struct FormatSpec
{
    bool padZeroes;
    NumberRepr repr;
}

private struct Format
{
    enum Type
    {
        invalid,
        literal,
        asString,
        asUnsigned,
        asSigned,
        asHex,
    }

    Type type;
    string value;
}

private struct FormatRange
{
    string input;
    Format front;

    this(string input) @nogc nothrow
    {
        this.input = input;
        if (input.length > 0)
            popFront();
    }

    bool empty() @nogc nothrow
    {
        return input.length == 0;
    }

    void popFront() @nogc nothrow
    {
        import std.algorithm : countUntil;

        auto count = (cast(ubyte[]) input).countUntil('%');
        if (count == 0 && input.length > count)
        {
            // parse format
            switch (input[1])
            {
            case 's':
                front.type = Format.Type.asString;
                front.value = input[0 .. 2];
                break;
            case 'u':
                front.type = Format.Type.asUnsigned;
                front.value = input[0 .. 2];
                break;
            case 'd':
                front.type = Format.Type.asSigned;
                front.value = input[0 .. 2];
                break;
            case 'x':
                front.type = Format.Type.asHex;
                front.value = input[0 .. 2];
                break;
            default:
                front.type = Format.Type.invalid;
                front.value = input[0 .. 2];
                break;
            }
            input = input[2 .. $];
        }
        else if (count > 0)
        {
            front.type = Format.Type.literal;
            front.value = input[0 .. count];
            input = input[count .. $];
        }
        else // count == -1
        {
            front.type = Format.Type.literal;
            front.value = input;
            input = input[$ .. $];
        }
    }
}

private struct FmtRange
{
    string input;
    string prefix;
    string fmt;

    this(string input) @nogc nothrow
    {
        this.input = input;

        findNextFmt();
    }

    bool empty() @nogc nothrow
    {
        return input.length == 0 && prefix.length == 0 && fmt.length == 0;
    }

    void findNextFmt() @nogc nothrow
    {
        size_t count;
        prefix = [];
        fmt = [];
        string temp = input;
        while (temp.length != 0)
        {
            if (temp[0] == '%')
            {
                auto index = 1;
                while (index < temp.length)
                {
                    if (temp[index] == 's' || temp[index] == 'd'
                            || temp[index] == 'u' || temp[index] == 'x')
                    {
                        fmt = temp[0 .. index + 1];
                        input = input[prefix.length + index + 1 .. $];
                        return;
                    }
                    index += 1;
                }
                ASSERT(false, "format has no type specifier");
            }

            temp = temp[1 .. $];
            count += 1;
            prefix = input[0 .. count];
        }
        input = input[prefix.length .. $];
    }
}

struct Formatter
{
    char[] buffer;
    size_t count;

    // TODO implement sumtype support
    void formatValue(T)(T value, string fmt = "%s") @nogc nothrow
    {
        import std.traits : isArray, isPointer;

        static if (is(T == enum))
            formatEnum(value, fmt);
        else static if (isIntegral!T)
            formatInteger(value, fmt);
        else static if (isFloatingPoint!T)
            formatFloat(value, fmt);
        else static if (is(T == struct))
            formatStruct(value, fmt);
        else static if (isPointer!T)
            formatPointer(value, fmt);
        else static if (is(T == string))
            formatString(value, fmt);
        else static if (isArray!T) // isArray matches string
            formatArray(value, fmt);
        else static if (is(T == union))
            formatString(T.stringof ~ "(union)", "%s");
        else
            static assert(false, "unsupported type: " ~ T.stringof);
    }

    void formatEnum(T)(T value, string fmt) @nogc nothrow
    {
        import std.traits : EnumMembers, OriginalType;

        static foreach (i, mem; EnumMembers!T)
        {
            if (mem == value)
            {
                enum Name = __traits(identifier, EnumMembers!T[i]);
                formatString(Name, "%s");
                return;
            }
        }
        formatValue(cast(OriginalType!T) value, fmt);
    }

    void formatInteger(T)(T value, string fmt) @nogc nothrow
    {
        if (fmt == "%s")
        {
            import std.traits : Unqual;

            alias UT = Unqual!T;
            static if (is(UT == byte))
                enum PrintfFmt = "%hhd";
            else static if (is(UT == ubyte))
                enum PrintfFmt = "%hhu";
            else static if (is(UT == short))
                enum PrintfFmt = "%hd";
            else static if (is(UT == ushort))
                enum PrintfFmt = "%hu";
            else static if (is(UT == int))
                enum PrintfFmt = "%d";
            else static if (is(UT == uint))
                enum PrintfFmt = "%u";
            else static if (is(UT == long))
                enum PrintfFmt = "%lld";
            else static if (is(UT == ulong))
                enum PrintfFmt = "%llu";
            else
                static assert(false, "unsupported integer type: " ~ T.stringof);

            printValue(PrintfFmt, value);
        }
        else
        {
            printValue(fmt, value);
        }
    }

    void formatFloat(T)(T value, string fmt) @nogc nothrow
    {
        if (fmt == "%s")
        {
            import std.traits : Unqual;

            alias UT = Unqual!T;
            static if (is(UT == float) || is(UT == double))
                enum PrintfFmt = "%f";
            else
                static assert(false, "type 'real' is unsupported");

            printValue(PrintfFmt, value);
        }
        else
        {
            printValue(fmt, value);
        }
    }

    void formatStruct(T)(T value, string fmt) @nogc nothrow
    {
        ASSERT(fmt == "%s", "unsupported format for structs");
        import std.traits;

        enum NAME = __traits(identifier, T);
        pragma(msg, "struct name: " ~ NAME);
        formatString(NAME, "%s");
        formatString(" { ", "%s");
        foreach (i, mem; FieldNameTuple!T)
        {
            if (i > 0)
                formatString(", ", "%s");
            pragma(msg, "struct member: " ~ mem);
            formatString(mem ~ ": ", "%s");
            //formatValue(mixin("value." ~ mem), "%s");
            formatValue(__traits(getMember, value, mem), "%s");
        }
        formatString(" }", "%s");
    }

    void formatArray(T)(T value, string fmt) @nogc nothrow
    {
        ASSERT(fmt == "%s", "unsupported format for arrays");
        formatChar('[', "%s");
        foreach (i, val; value)
        {
            if (i > 0)
                formatString(", ", "%s");
            formatValue(val, "%s");
        }
        formatChar(']', "%s");
    }

    void formatPointer(T)(T value, string fmt) @nogc nothrow
    {
        ASSERT(fmt == "%s", "unsupported format for pointers");
        enum NAME = T.stringof;
        formatString("(" ~ NAME ~ ")", "%s");
        printValue("0x%llx", cast(ulong) value); // string literals are \0-terminated
    }

    void formatChar(char c, string fmt) @nogc nothrow
    {
        ASSERT(fmt == "%s" || fmt == "%c", "unsupported format for chars");

        printValue("%c", c);
    }

    void formatString(string value, string fmt) @nogc nothrow
    {
        ASSERT(fmt == "%s", "unsupported format for strings");

        printValue("%.*s", cast(int) value.length, value.ptr);
    }

    private void printValue(Args...)(string fmt, Args args) @nogc nothrow
    {
        char[32] fmtBuffer = '\0';
        ASSERT(fmt.length < 32, "format too long");
        fmtBuffer[0 .. fmt.length] = fmt;

        import core.stdc.stdio : snprintf;

        auto res = snprintf(buffer.ptr + count, buffer.length - count, fmt.ptr, args);
        if (res < 0)
        {
            ASSERT(false, "snprintf error");
        }
        else if (res >= buffer.length - count)
        {
            ASSERT(false, "not enough space in buffer for formatted value");
        }
        count += res;
    }
}

unittest
{
	// struct test
	static struct Test1
	{
		int x = 4;
	}
	Log.dbg("test struct: %s", Test1());

	static struct Test2
	{
		int x = 4;
		private int y = 5;
	}
	Log.dbg("test struct 2: %s", Test2());
}
