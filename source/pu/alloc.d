module pu.alloc;

import pu.assert_;

//private __gshared Allocator _allocator;
//
//alias FreeFun = void function(void* ptr, void* userdata);
//alias AllocFun = void* function(size_t size, void* userdata);
//alias ReallocFun = void* function(void* ptr, size_t size, void* userdata);
//
//struct Allocator
//{
//	void* userdata;
//	FreeFun free;
//	AllocFun alloc;
//	ReallocFun realloc;
//}

T* alloc(T, Args...)(auto ref Args args) @nogc nothrow if(!is(T == class))
{
	import core.stdc.stdlib : malloc;
	auto ptr = cast(T*)malloc(T.sizeof);
	if(!ptr)
	{
		ASSERT(false, "malloc returned null pointer");
	}
	import core.lifetime : emplace;
	ASSERT(emplace(ptr, args) == ptr, "failed to emplace object");
	return ptr;
}

T* allocArray(T)(size_t num) @nogc nothrow if(!is(T == class))
{
	import core.stdc.stdlib : malloc;
	auto ptr = cast(T*)malloc(T.sizeof * num);
	if(!ptr)
	{
		ASSERT(false, "malloc returned null pointer");
	}
	foreach(i; 0..num)
	{
		import core.lifetime : emplace;
		ASSERT(emplace(ptr+i) == ptr+i, "failed to emplace object");
	}
	return ptr;
}

void free(T)(T* ptr) @nogc nothrow if(!is(T == class))
{
	import core.stdc.stdlib : free;
	ASSERT(ptr, "trying to free null pointer");
	free(ptr);
}

